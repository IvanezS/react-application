import LoginReducer from './Authentification/LoginReducer'
import usersReducer from './Components/UsersSlice'

export default function rootReducer(state ={} , action) {
  // always return a new object for the root state
  return {
    login: LoginReducer(state.login, action),
    users: usersReducer(state.users, action)
  }
}