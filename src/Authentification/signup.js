import React, { Component } from "react";
import { Button, Jumbotron,  Container} from 'react-bootstrap'

export default class SignUp extends Component {
    render() {
        return (
            <form>

                <Jumbotron fluid >
                    <Container>
                        <h1 className="display-4">Зарегистрироваться</h1>
                    </Container>
                </Jumbotron>

                <div className="form-group">
                    <label>Имя</label>
                    <input type="text" className="form-control" placeholder="Введите имя" />
                </div>

                <div className="form-group">
                    <label>Фамилия</label>
                    <input type="text" className="form-control" placeholder="Введите фамилию" />
                </div>

                <div className="form-group">
                    <label>Email</label>
                    <input type="email" className="form-control" placeholder="Введите email" />
                </div>

                <div className="form-group">
                    <label>Пароль</label>
                    <input type="password" className="form-control" placeholder="Введите пароль" />
                </div>

                <button type="submit" className="btn btn-primary btn-block">Зарегистрироваться</button>
                <p className="forgot-password text-right">
                    Уже зарегистрирован <a href="#">Войти?</a>
                </p>
            </form>
        );
    }
}