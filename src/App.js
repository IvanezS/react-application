import GetUsers from './Components/UserList';
import React from 'react';

import { BrowserRouter as Router, Switch, Route, } from 'react-router-dom';
import { LinkContainer } from "react-router-bootstrap";
import { Navbar, Nav, NavDropdown } from 'react-bootstrap'

import Login from "./Authentification/login";
import SignUp from "./Authentification/signup";


function App() {
  return (
    

    <Router>

    <div className="container">

     
        <Navbar bg="light" variant="light" expand="lg" sticky="top">
          <Navbar.Brand>React + Redux</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">

                <LinkContainer to="/HomePage ">
                  <NavDropdown.Item>Пользователи</NavDropdown.Item>
                </LinkContainer>



            </Nav>

            <Nav>
                <LinkContainer to="/sign-in">
                  <NavDropdown.Item>Войти</NavDropdown.Item>
                </LinkContainer>

                <LinkContainer to="/sign-up">
                  <NavDropdown.Item>Зарегистрироваться</NavDropdown.Item>
                </LinkContainer>
          </Nav>
          </Navbar.Collapse>
        </Navbar>
      

      <Switch>
        <Route path='/HomePage ' component={GetUsers}></Route>
        <Route exact path='/' component={Login} />
        <Route path="/sign-in" component={Login} />
        <Route path="/sign-up" component={SignUp} />
      </Switch>

    </div>

  </Router>



  );
}

export default App;
