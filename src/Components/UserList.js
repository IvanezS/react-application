import React from 'react';
import { Button, Jumbotron,  Container} from 'react-bootstrap'
import { selectAllUsers, fetchUsers } from './UsersSlice'
import { useDispatch, useSelector } from 'react-redux'

function GetUsers() {
    const dispatch = useDispatch()
    const users = useSelector(selectAllUsers)
    const usersStatus = useSelector((state) => state.users.status)
    const error = useSelector((state) => state.users.error)

    function getU() {
        dispatch(fetchUsers())
    }

    return(
        <div>
            <Jumbotron fluid >
                <Container>
                    <h1 className="display-4">Пользователи</h1>
                </Container>
            </Jumbotron>

            <Button variant="success" onClick = {getU}>жми</Button>
            <ul>
                {users.users && users.users.map(function(el,index) {
                    return <li key={index}>{"Имя: " + el.name + " ---> email: " + el.email}</li>
                })}
            </ul>
        </div>
    );

}
export default GetUsers