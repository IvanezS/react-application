import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Button, Jumbotron,  Container, Form, FormGroup } from 'react-bootstrap'
import { store } from "../store";

export default function Login()  {
    
    const dispatch = useDispatch();
    const [LoginInfo, setLoginInfo] = useState({
        email: '',
        password: ''
    });



    const handleChange = (e) => {
        debugger
        setLoginInfo({...LoginInfo, [e.target.name]: e.target.value}, () => {
            console.log(this.state);});
        dispatch({ type: 'LOGINPASSWORDCHANGED', payload: LoginInfo})

    }

    
    const handleSubmit = (event) => {
        alert('Отправлены email: ' + this.state.email + ' и пароль: ' + this.state.password);
        event.preventDefault();
    };

    
    return (
            

            <Form  onSubmit={handleSubmit}>

                <Jumbotron fluid >
                    <Container>
                        <h1 className="display-4">Войти</h1>
                    </Container>
                </Jumbotron>

                <FormGroup >
                    <label>Email</label>
                    <input type="email" name = "email" className="form-control" placeholder="Введите email" value={LoginInfo.email} onChange={handleChange} />
                </FormGroup >

                <FormGroup >
                    <label>Пароль</label>
                    <input type="password" name = "password" className="form-control" placeholder="Введите пароль"  value={LoginInfo.password} onChange={handleChange}/>
                </FormGroup >

                <FormGroup >
                    <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customCheck1" />
                        <label className="custom-control-label" htmlFor="customCheck1">Запомнить меня</label>
                    </div>
                </FormGroup >

                <Button type="submit" className="btn btn-primary btn-block">Войти</Button>
                <p className="forgot-password text-right">
                    Забыли <a href="#">пароль?</a>
                </p>
            </Form>
    );
    
}