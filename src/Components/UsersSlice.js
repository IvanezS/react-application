import axios from "axios";
import { createSlice, nanoid, createAsyncThunk } from '@reduxjs/toolkit'

const initialState = {
  users: [],
  status: 'idle',
  error: null
}

export const fetchUsers = createAsyncThunk(
  'GET_USERS',
  async () => {
    const response = await axios.get('/public-api/users');
    return response.data
  }
)


const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {},
  extraReducers: {
    [fetchUsers.pending]: (state, action) => {
      state.status = 'loading'
    },
    [fetchUsers.fulfilled]: (state, action) => {
      state.status = 'succeeded'
      state.users = action.payload.data
    },
    [fetchUsers.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.error.message
    }
  }
})

export default usersSlice.reducer

export const selectAllUsers = (state) => {
  return state.users
}
