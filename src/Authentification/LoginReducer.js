export default function LoginReducer(state = '', action) {
  console.log("action =", action)
  switch (action.type) {
    case 'LOGINPASSWORDCHANGED': {
      return [
        ...state,
        action.payload,
      ]
    }

    default:
      return state
  }
}